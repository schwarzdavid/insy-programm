var express = require('express'),
	mongoose = require('mongoose'),
	router = express.Router();

router.get('/', function(req, res){
	mongoose.model('hersteller').find({}, function(err, result){
		if(err){
			console.log(err);
			return res.status(500).send('Internal Server Error');
		}
		
		res.render('index', {result:result});
	});
});

router.use('/hersteller', require('./hersteller.js'));

router.all('*', function(req, res){
	res.send('Bad Route');
});

module.exports = router;