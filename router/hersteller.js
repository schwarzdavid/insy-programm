var express = require('express'),
	mongoose = require('mongoose'),
	hersteller = mongoose.model('hersteller'),
	router = express.Router();

router.route('/').get(function(req, res){
	res.render('addHersteller');
}).post(function(req, res){
	var newHersteller = new hersteller({
		name: req.body.hersteller
	});
	
	newHersteller.save(function(err, data){
		if(err){
			console.log(err);
			return res.send('Fehler bei DB Insert. Check Console Log');
		}
		
		res.redirect('/');
	});
});

router.route('/del/:id').get(function(req, res){
	hersteller.remove({
		_id: req.params.id
	}, function(err, removed){
		if(err){
			console.log(err);
			return res.status(500).send('Fehler bei DB Delete');
		}
		
		res.redirect('/');
	});
});

router.route('/autos/:id').get(function(req, res){
	hersteller.findById(req.params.id, function(err, result){
		if(err){
			console.log(err);
			return res.status(404).send('Fehler bei DB Abfrage. Der Hersteller existiert nicht');
		}
		
		res.render('showHersteller', { result:result });
	});
});

router.route('/autos/:id/add').get(function(req, res){
	res.render('autoHinzufuegen', {url:req.params.id});
}).post(function(req, res){
	hersteller.findById(req.params.id, function(err, result){
		if(err){
			console.log(err);
			return res.status(404).send('Fehler bei DB Abfrage. Dieser Hersteller existiert nicht');
		}
		
		result.models.push({ 
			name:req.body.name,
			verbrauch:req.body.verbrauch
		});
		
		result.save(function(err){
			if(err){
				console.log(err);
				return res.status(500).send('Fehler bei Insert von neuem Modell');
			}
			
			res.redirect('/hersteller/autos/'+req.params.id);
		});
	});
});

router.route('/autos/:hersteller/delete/:auto').get(function(req, res){
	hersteller.findById(req.params.hersteller, function(err, result){
		if(err){
			console.log(err);
			return res.status(500).send('Fehler bei DB Select');
		}
		
		result.models.id(req.params.auto).remove();
		result.save(function(err){
			if(err){
				console.log(err);
				return res.status(500).send('Fehler bei DB Update');
			}
			
			res.redirect('/hersteller/autos/'+req.params.hersteller);
		});
	});
});

module.exports = router;