var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	model;

model = new Schema({
	name: {
		type: String,
		required: true,
		trim: true
	},
	
	createDate: Date,
	
	models: [{
		name:String,
		verbrauch:Number
	}]
});

model.pre('save', function(next){
	if(!this.createDate){
		this.createDate = Date.now();
	}
	next();
});

module.exports = model;