var express = require('express'),
	mongoose = require('mongoose'),
	bodyParser = require('body-parser'),
	fs = require('fs'),
	app = express();

app.set('views', './views');
app.set('view engine', 'ejs');

app.use(express.static('./bower_components/'));
app.use(express.static('./public/'));

app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(bodyParser.json());

mongoose.connect('mongodb://localhost/spielwiese', function(err){
	if(err){
		throw new Error(err);
	}
	
	mongoose.model('hersteller', require('./schemas/hersteller.js'));
	
	app.use(require('./router/main.js'));
	
	app.listen('3000', function(err){
		if(err){
			throw new Error(err);
		}
		
		console.log('Bam it works');
	});
});